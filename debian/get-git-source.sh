#!/bin/sh

set -ex

PACKAGE=sshcommand
BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9]\)\+.*/\1/p')
OLDCWD="${PWD}"
GOS_DIR="${OLDCWD}/get-orig-source"
GIT_DESCRIBE_STR='git describe --always'
TODAY="$(date '+%Y%m%d')"

if [ -z "${BASE_REL}" ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi


mkdir "${GOS_DIR}"
git clone git@github.com:dokku/sshcommand.git "${GOS_DIR}/${PACKAGE}"
cd "${GOS_DIR}/${PACKAGE}"
GIT_DESCRIBE=$(eval "${GIT_DESCRIBE_STR}")
cd "${GOS_DIR}"
XZ_OPT=-f9 tar cJf \
	"${OLDCWD}/${PACKAGE}_${BASE_REL}~${TODAY}.1~${GIT_DESCRIBE}.orig.tar.xz" \
	"${PACKAGE}" --exclude-vcs
cd "${OLDCWD}"
